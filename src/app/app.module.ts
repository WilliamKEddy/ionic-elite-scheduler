import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MyTeamsPageModule } from '../pages/my-teams/my-teams.module';
import { TournamentsPageModule } from '../pages/tournaments/tournaments.module';
import {TeamsPageModule} from "../pages/teams/teams.module";
import {TeamDetailPageModule} from "../pages/team-detail/team-detail.module";
import {TeamHomePageModule} from "../pages/team-home/team-home.module";
import {StandingsPageModule} from "../pages/standings/standings.module";
import { HttpModule } from '@angular/http';
import { GamePageModule } from '../pages/game/game.module';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    MyTeamsPageModule,
    TeamsPageModule,
    TeamDetailPageModule,
    TeamHomePageModule,
    StandingsPageModule,
    TournamentsPageModule,
    GamePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
