import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class EliteApi {

  private baseUrl = 'https://elite-schedule-app-i2-265ec.firebaseio.com';
  currentTournament: any = {};

  constructor(private http: Http) {
  }

  getTournaments() {
    const dataUrl =`${this.baseUrl}/tournaments.json`;
    return this.http.get(dataUrl)
               .map((response: Response) => {
                  return response.json();
    });
  }

  getTournamentData(tournamentId): Observable<any> {
    const dataUrl =`${this.baseUrl}/tournaments-data/${tournamentId}.json`;
    return this.http.get(dataUrl)
        .map((response: Response) => {
          this.currentTournament = response.json();
          return this.currentTournament;
    });
  }

  getCurrentTournament() {
    return this.currentTournament;
  }
}
