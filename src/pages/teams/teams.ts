import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';

import * as _ from 'lodash';

import { TeamHomePage } from '../pages';
import { EliteApi } from '../../shared/shared';

@IonicPage()
@Component({
  selector: 'page-teams',
  templateUrl: 'teams.html',
})
export class TeamsPage {

  private allTeams: any;
  private allTeamDivisions: any;
  divisions: any[] = [];

  constructor(private nav: NavController,
              private navParams: NavParams,
              private eliteApi: EliteApi,
              private loadingController: LoadingController) {
  }

  ionViewDidLoad() {
    const selectedTournament = this.navParams.data;

    const loader = this.loadingController.create({
      content: 'Getting data...'
    });
    loader.present().then(() => {
      this.eliteApi.getTournamentData(selectedTournament.id)
          .subscribe(data => {
            this.allTeams = data.teams;
            this.allTeamDivisions =
              _.chain(data.teams)
               .groupBy('division')
               .toPairs()
               .map(item => _.zipObject(['divisionName', 'divisionTeams'], item))
               .value();
            this.divisions = this.allTeamDivisions;
            loader.dismiss();
          });
    });

  }

  itemTapped($event, team) {
    this.nav.push(TeamHomePage, team);
  }

}
