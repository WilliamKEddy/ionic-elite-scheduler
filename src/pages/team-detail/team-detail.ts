import { Component } from '@angular/core';
import { ToastController, AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';

import * as _ from 'lodash';
import * as moment from 'moment';

import { GamePage } from '../pages';
import { EliteApi } from '../../shared/shared';

@IonicPage()
@Component({
  selector: 'page-team-detail',
  templateUrl: 'team-detail.html',
})
export class TeamDetailPage {

  isFollowing = false;
  dateFilter: string;
  useDateFilter = false;
  allGames: any[];
  games: any[];
  team: any;
  teamStanding: any;
  private tournamentData: any;

  constructor(private nav: NavController,
              private alertController: AlertController,
              private toastController: ToastController,
              private navParams: NavParams,
              private eliteApi: EliteApi) {
  }

  ionViewDidLoad() {
    this.team = this.navParams.data;
    this.tournamentData = this.eliteApi.getCurrentTournament();

    this.games = _.chain(this.tournamentData.games)
                  .filter(g => g.team1Id === this.team.id || g.team2Id === this.team.id)
                  .map(g => {
                    const isTeam1 = (g.team1Id === this.team.id);
                    const opponentName = isTeam1 ? g.team2 : g.team1;
                    const scoreDisplay = this.getScoreDisplay(isTeam1, g.team1Score, g.team2Score);
                    return {
                      gameId: g.id,
                      opponent: opponentName,
                      time: Date.parse(g.time),
                      location: g.location,
                        locationUrl: g.locationUrl,
                      scoreDisplay: scoreDisplay,
                      homeAway: (isTeam1 ? 'vs.' : 'at')
                    };
                  })
                  .value();

    this.allGames = this.games;
    this.teamStanding = _.find(this.tournamentData.standings, {'teamId':this.team.id});
  }

  gameClicked($event, game) {
    const sourceGame = this.tournamentData.games.find(g => g.id === game.gameId);
    this.nav.parent.parent.push(GamePage, sourceGame);
  }

  dateChanged() {
    if(this.useDateFilter){
      this.games = _.filter(this.allGames, g => moment(g.time).isSame(this.dateFilter, 'day'));
    } else {
      this.games = this.allGames;
    }
  }

  getScoreWorL(game) {
    return game.scoreDisplay ? game.scoreDisplay[0] : '';
  }

  getScoreDisplayBadgeClass(game) {
    return game.scoreDisplay.indexOf('W:') === 0 ? 'primary' : 'danger';
  }

  toggleFollow() {
    const teamName = this.team.name;
    if(this.isFollowing){
      let confirm = this.alertController.create({
        title: 'Unfollow?',
        message: 'Are you sure you want to unfollow?',
        buttons: [
          {
            text: 'Yes',
            handler: () => {
              this.isFollowing = false;
              // TODO: persist data

              const toast = this.toastController.create({
                message: `You are no longer following ${teamName}.`,
                duration: 2000,
                position: 'bottom'
              });
              toast.present();
            }
          },
          {
            text: 'No'
          }
        ]
      });
      confirm.present();
    } else {
      this.isFollowing = true;
      // TODO: persist data
    }
  }

  private getScoreDisplay(isTeam1: boolean, team1Score: any, team2Score: any) {
    if(team1Score && team2Score) {
      const teamScore = (isTeam1 ? team1Score : team2Score);
      const opponentScore = (isTeam1 ? team2Score: team1Score);
      const winIndicator = teamScore > opponentScore ? 'W: ' : 'L: ';
      return winIndicator + teamScore + '-' + opponentScore;
    }else {
      return '';
    }
  }
}
