import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { EliteApi } from '../../shared/shared';
import { TeamHomePage } from '../pages';

/**
 * Generated class for the GamePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-game',
  templateUrl: 'game.html',
})
export class GamePage {

  game: any;

  constructor(private nav: NavController,
              private navParams: NavParams,
              private eliteApi: EliteApi) {
    this.game = this.navParams.data;
  }

  ionViewDidLoad() {
  }

  teamTapped(teamId: string) {
    const tournamentData = this.eliteApi.getCurrentTournament();
    const team = tournamentData.teams.find(t => t.id === teamId);
    this.nav.push(TeamHomePage, team);
  }

}
