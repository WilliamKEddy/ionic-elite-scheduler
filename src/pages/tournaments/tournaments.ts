import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';

import { TeamsPage } from '../pages';
import { EliteApi } from '../../shared/shared';

@IonicPage()
@Component({
  selector: 'page-tournaments',
  templateUrl: 'tournaments.html',
})
export class TournamentsPage {

  tournaments: any;

  constructor(public nav: NavController,
              public navParams: NavParams,
              private eliteApi: EliteApi,
              private loadingController: LoadingController) {
  }

  itemTapped($event, item) {
    this.nav.push(TeamsPage, item);
  }

  ionViewDidLoad() {
    const loader = this.loadingController.create({
      content: 'Getting tournaments...'
    });

    loader.present().then(()=>{
      this.eliteApi.getTournaments()
          .subscribe(data => {
            this.tournaments = data;
            loader.dismiss();
          });
    });
  }
}
